# Getting Started

To develop plugins for MinecraftOnline, you must have a basic understanding of programming principles.
Specific knowledge of programming in Java is highly recommended.
Other languages we use include Bash and Python.

The Development team suggests taking the following steps to get setup for developing for MinecraftOnline.

1. Download one of the following suitable Integrated Development Environments (IDE) for plugin development:
  * **Suggested:** JetBrain's [IntelliJ IDEA](https://www.jetbrains.com/idea/)
  * [Eclipse](https://www.eclipse.org/ide/)
  * [Visual Studio](https://visualstudio.microsoft.com/)
1. Download a suitable text editor for scripts and miscellaneous development:
  * Simple, easy to use:
    * [Atom](https://atom.io/)
    * [Notepad++](https://notepad-plus-plus.org/downloads/)
    * [Sublime Text](https://www.sublimetext.com/)
  * Powerful, universal, difficult to learn
    * [Vim](https://www.vim.org/)
1. Apply Add-ons to IDE:
  * IntelliJ Plugins (restart after downloading):
    1. [Minecraft Development](https://plugins.jetbrains.com/plugin/8327-minecraft-development) (by DemonWav)
    1. [Checkstyle-IDEA](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea) (by Jamie Shiell)
1. Skim [Sponge Documentation](https://docs.spongepowered.org/)
1. Request access to desired projects by [contacting admins](mailto:admins@minecraftonline.com)

## Java Plugin Conventions

When programming in Java, it is highly recommended you use
[Sun's](https://checkstyle.sourceforge.io/sun_style.html) or
[Google's](https://checkstyle.sourceforge.io/styleguides/google-java-style-20180523/javaguide.html)
code style checks.
In IntelliJ, these checks can be enforced through the Checkstyle-IDEA plugin by going to
`File -> Settings -> Checkstyle`. These style checks include enforcing java docs for public
methods and classes, proper spacing and line lengths, and variable name conventions, among others.

Every Sponge Plugin being developed for MCO must contain at least the following:
1. readme.md (give any Admins or new developers a place to start)
  - [ ] Purpose
  - [ ] Current Status
  - [ ] Context (related plugins, scripts, important dependencies)
  - [ ] Future plans (is this going to be replaced by something else in the past?)
  - [ ] Past projects (has there been anything else that fulfilled this project's purpose previously?)
1. contributing.md (even if you're the only one working on the project, someone else might work on it later)
  - [ ] Style guide in use (ideally Sun's or Google's)
  - [ ] Instructions to begin editing
  - [ ] Instructions to build/test
  - [ ] Instructions to submit changes

## Script Conventions

**In Progress**
