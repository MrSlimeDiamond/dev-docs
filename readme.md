
Built with GitBook and GitLab pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

# Welcome

These pages are meant to provide documentation to the development process at MinecraftOnline,
the oldest running multiplayer Minecraft Server to date. The server is constantly changing as
developers add new features and waves of players and staff come and go.
The server prides itself with its dynamism and abundance of custom features.

However, because the server has so many custom components, 
updating to newer versions of Minecraft can prove challenging.
To provide a centralization for development efforts and conventions and to promote efficiency,
these pages should tie current and future members of the team together.

# MinecraftOnline Development

There are many projects currently underway behind the scenes at MCO.
As of Minecraft server version 1.7.10, the server runs on [CanaryMod](https://canarymod.net/) 
and add custom functionality using:
* Canary Plugins (Java)
* Scripts handling commands (Bash, Python)
* ...

As of Minecraft server version 1.12.2, the server runs on [Sponge](https://www.spongepowered.org/) and uses:
* Sponge plugins (Java)
* Scripts?
* ...

Additional components used by MinecraftOnline as an organization include:
* Website (Language?)
* Scripts (Language?)
