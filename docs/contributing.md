# Contributing To MCO Documentation

Any official MCO documentation hosted here on GitLab follows
the same contribution guidelines,
given you have access to the repository.
These are a few options available to you if you want to
contribute to any of these documentation pages.

1. Simply [email the admin team](mailto:admins@minecraftonline.com) to suggest changes indirectly.
1. Use the Web IDE offered by GitLab
1. Use a local text editor on your machine

If GitBook is unclear to you, see the
[GitBookIO documentation](https://gitbookio.gitbooks.io/documentation/)

#### GitLab's Web IDE
This option is best for users without much experience with git
and if you have a small number of edits to make.
You will be able to do most of the things you would normally do in a local text editor.
However, this provides certain limitations compared to mor powerful text editors
you can download on your local machine.

Like most IDE's, this one also offers the Preview screen
of the Markdown code written on the edited documents.
This is helpful, but **there is no way to compile the GitBook webpage**.
Therefore, your edits cannot be seen in context until added to the master branch.

#### Work Locally

To work locally on any MCO documentation project, you need to have at least
[a basic understanding of Git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
and you'll have to follow these steps.

1. If you haven't already, [install node.js and npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
1. Install GitBook: `npm install gitbook-cli -g`
1. Fetch GitBook's stable version 3.2.3: `gitbook fetch 3.2.3`
1. Fork or clone this project and navigate locally to the directory
1. Checkout `dev` branch: `git fetch` and `git checkout dev`
   or create and checkout a new branch
1. **Add or edit any desired content**
  * The names of added files must not contain any characters other than lower case letters and dashes.
  * All added files must be `.md`
1. Preview your project: `gitbook serve`
1. Generate the website: `gitbook build`
  (*optional*. This will generate the files which can load directly onto your browser locally
  without further help from GitBook. This doesn't affect the actual documentation website.)
1. Push your changes to the `dev` branch or your custom branch: `git push`
1. If you are working on a fork and are ready to share with origin/master,
   submit a pull request.

## Conventions/Tips

Please keep all new files organized in the appropriate folders
and name files lowercase and word delimited with a dash.

* **Yes**: getting-started.md
* No: gettingstarted.md
* No: gettingStarted.md
* No: getting-started.txt

When suggesting changes, keep your audience in mind
and ask yourself these questions:

> Is your addition too simple/complicated for the intended user?

> How important is this information for the intended user?

> What is the user's goal?

> How did the user get to this page/location?
