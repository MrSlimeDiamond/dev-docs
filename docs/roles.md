# Developer Roles

| Role | Username |
| :-- | :-- |
| Lead Developer | 14mRh4X0r |
| Plugin - Hermes | techkid6 |
| Plugin - Nope | tyhdefu |
| Plugin - GriefAlert | PietElite |
| Plugin - Exec | techkid6 |
| Plugin - MCO Core | 14mRh4X0r |
| Plugin - FishyShield | doublehelix457 |
| Plugin - CommandOn | techkid6 |
| Plugin - Intake Forge | 14mRh4X0r |
| Plugin - MCO Database | 14mRh4X0r |
| Plugin - Echelon | 14mRh4X0r |
| Plugin - Charon | 14mRh4X0r |
| Plugin - Craftbook Extra | doublehelix457 |
| Plugin - LWC | doublehelix457 |
| Plugin - BorderLands | **OPEN** |
| Plugin - WorldEdit | **OPEN** |
| Plugin - LionsShop | **OPEN** |
| Plugin - LionsCrafting | **OPEN** |
| Plugin - LionsChips | **OPEN** |
| Plugin - VoxelSniper | **OPEN** |
| Plugin - Oogah | **OPEN** |
| Plugin - Worldguard | **OPEN** |
| Repository Librarian | ServL |
| Talent Acquisition | ServL |
| New Website | **OPEN** |
